/*Rodrigo Rivas -- ID: 1910674*/
package LinerAlgebra;

public class Vector3d {
	private double x;
	private double y;
	private double z;
	
	public Vector3d(double x, double y, double z){
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}

	public double magnitude() {	
		return Math.sqrt(Math.pow(x, 2) + Math.pow(y,2) + Math.pow(z, 2));	 
	}
	
	public double dotProduct(Vector3d vec) {
		double x1 = vec.getX();
		double y1 = vec.getY();
		double z1 = vec.getZ();
		
		return (x*x1) + (y*y1) + (z*z1);
	}
	
	public Vector3d add(Vector3d vec) {
		double x1 = vec.getX();
		double y1 = vec.getY();
		double z1 = vec.getZ();
		
		Vector3d newvec = new Vector3d(x+x1,y+y1,z+z1);
		
		return newvec;
	}
	
	public String toString() {
		return "("+x+","+y+","+z+")";
	}
}
