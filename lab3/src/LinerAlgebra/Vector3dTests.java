/*Rodrigo Rivas -- ID: 1910674*/
package LinerAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	void testCreateVector() {
		Vector3d v1 = new Vector3d(1,1,2);
		assertEquals(1, v1.getX());
		assertEquals(1, v1.getY());
		assertEquals(2, v1.getZ());
	}
	
	@Test
	void testMagnitud() {
		Vector3d v1 = new Vector3d(1,1,2);
		assertEquals(2.449, v1.magnitude(),2);
	}
	
	@Test
	void testDotProduct() {
		Vector3d v1 = new Vector3d(1,1,2);
		Vector3d v2 = new Vector3d(2,3,4);
		assertEquals(13, v1.dotProduct(v2));
	}
	
	@Test
	void testAdd() {
		Vector3d v1 = new Vector3d(1,1,2);
		Vector3d v2 = new Vector3d(2,3,4);
		Vector3d test = v1.add(v2);
		// Vector3d(3,4,6);
		
		assertEquals(3, test.getX());
		assertEquals(4, test.getY());
		assertEquals(6, test.getZ());
	}

}
